package com.vms.util;

public class Query {
	public static String adminAuth="Select * from admin where uid=? and password=?";
	public static String addVenue="insert into venue_details values(?,?,?)";
	public static String editVname="update venue_details set vname=? where vid=?";
	public static String editCity="update venue_details set city=? where vid=?";
	public static String editVnamecity="update venue_details set vname=?,city=? where vid=?";
	public static String view="Select * from venue_details";
	public static String removeVenue="delete from venue_details where vid=?";
	
}
