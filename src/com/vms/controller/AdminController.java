package com.vms.controller;

import java.util.List;

import com.vms.dao.IAdminDao;
import com.vms.dao.IAdminImpl;
import com.vms.model.Admin;
import com.vms.model.VenueDetails;

public class AdminController {
	int result;
	IAdminDao dao=new IAdminImpl();
	public int adminAuthentication(String uid,String password) {
		Admin admin=new Admin(uid,password);
		return dao.adminAuthentication(admin);
	}
	public int addVenue(int vid, String vname, String city) {
		VenueDetails details=new VenueDetails(vid,vname,city);
		return dao.addVenue(details);
	}
	public int editVname(int vid,String vname) {
		VenueDetails details=new VenueDetails();
		details.setVid(vid);
		details.setVname(vname);
		return dao.editVname(details);	
		
	}
	public int editCity(int vid,String city) {
		VenueDetails details=new VenueDetails();
		details.setVid(vid);
		details.setVname(city);
		return dao.editCity(details);	
		
	}
	public int editVnamecity(int vid,String vname,String city) {
		VenueDetails details=new VenueDetails();
		details.setVid(vid);
		details.setVname(vname);
		details.setCity(city);
		return dao.editVnamecity(details);
	}
	
	public List<VenueDetails> viewAllVenues(){
		return dao.viewAllVenues();
	}
	public int removeVenue(int vid) {
		VenueDetails details=new VenueDetails();
		details.setVid(vid);
		return dao.removeVenue(details);
	}

}
