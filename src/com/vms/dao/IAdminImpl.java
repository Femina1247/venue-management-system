package com.vms.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vms.model.Admin;
import com.vms.model.VenueDetails;
import com.vms.util.Db;
import com.vms.util.Query;

public class IAdminImpl implements IAdminDao {
	PreparedStatement pst=null;
	ResultSet rs=null;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.adminAuth);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs=pst.executeQuery();
			while(rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in Admin Authentication");
		}finally {
		
		try {
			pst.close();
			rs.close();
			Db.getConnection().close();
		} catch (SQLException | ClassNotFoundException  e) {
			e.printStackTrace();
		}
		}
		return result;
	}

	@Override
	public int addVenue(VenueDetails details) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.addVenue);
			pst.setInt(1, details.getVid());
			pst.setString(2, details.getVname());
			pst.setString(3, details.getCity());
			result=pst.executeUpdate();
		}catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in add venue");
		}finally {
			
			try {
				Db.getConnection().close();
				pst.close();
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	
	}

	@Override
	public int editVname(VenueDetails details) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editVname);
			pst.setString(1, details.getVname());
			pst.setInt(2, details.getVid());
			result=pst.executeUpdate();
		}catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in edit venue name");
		}finally {
			
			try {
				Db.getConnection().close();
				pst.close();
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	   
	}
	@Override
	public int editCity(VenueDetails details) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editCity);
			pst.setString(1, details.getCity());
			pst.setInt(2, details.getVid());
			result=pst.executeUpdate();
		}catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in edit city");
		}finally {
			
			try {
				Db.getConnection().close();
				pst.close();
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}


	@Override
	public int editVnamecity(VenueDetails details) {
		
		
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editVnamecity);
			pst.setString(1, details.getVname());
			pst.setString(2, details.getCity());
			pst.setInt(3, details.getVid());
			result=pst.executeUpdate();
		}catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception in edit venue name and city");
		}finally {
			
			try {
				Db.getConnection().close();
				pst.close();
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<VenueDetails> viewAllVenues() {
		List<VenueDetails> list=new ArrayList<VenueDetails>();
		try {
			pst=Db.getConnection().prepareStatement(Query.view);
			rs=pst.executeQuery();
			while(rs.next()) {
				VenueDetails details=new VenueDetails(rs.getInt(1),rs.getString(2),rs.getString(3));
				list.add(details);
			}
		}catch(ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view all venues");
		}finally {
			try {
				Db.getConnection().close();
				rs.close();
				pst.close();
			}catch(ClassNotFoundException | SQLException e) {
		}
		}
		return list;
	}

	@Override
	public int removeVenue(VenueDetails details) {
			result=0;
			try {
				pst=Db.getConnection().prepareStatement(Query.removeVenue);
				pst.setInt(1, details.getVid());
				result=pst.executeUpdate();
			}catch(ClassNotFoundException | SQLException e) {
				System.out.println("Exception in remove venue");
			}finally {
				
				try {
					Db.getConnection().close();
					pst.close();
				} catch (SQLException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		return result;
	}

	
}
