package com.vms.dao;

import java.util.List;

import com.vms.model.Admin;
import com.vms.model.VenueDetails;

public interface IAdminDao {

	public int adminAuthentication(Admin admin);
	public int addVenue(VenueDetails details);
	public int editVname(VenueDetails details);
	public int editCity(VenueDetails details);
	public int editVnamecity(VenueDetails details);
	public List<VenueDetails> viewAllVenues();
	public int removeVenue(VenueDetails details);
}
