package com.vms.view;

import java.util.List;
import java.util.Scanner;

import com.vms.controller.AdminController;
import com.vms.model.VenueDetails;

public class Main {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter User Id");
		String uid=sc.nextLine();
		System.out.println("Enter password");
		String password=sc.nextLine();
		AdminController controller=new AdminController();
		int result=0;
		result=controller.adminAuthentication(uid,password);
		if(result>0) {
			System.out.println("Welcome to admin page");
			int cont=0;
			do {
			System.out.println("1.Add venues 2.Remove venues 3.Edit venues 4.View all");
			int option=sc.nextInt();
			switch(option) {
			case 1:
				System.out.println("Enter venue Id");
				int vid=sc.nextInt();
				sc.nextLine();
				System.out.println("Enter venue name");
				String vname=sc.nextLine();
				System.out.println("Enter city");
				String city=sc.nextLine();
				result=controller.addVenue(vid,vname, city);
				if(result>0) {
					System.out.println(vid +" Added Successfully!!!");
				}
			break;
			case 2:
				System.out.println("Enter Venue Id to remove");
				vid=sc.nextInt();
				result=controller.removeVenue(vid);
				System.out.println((result>0) ? vid+" Venue removed sucessfully ": vid+" Venue not Removed");
				
				break;
			case 3:
				System.out.println("1.Edit Vname, 2.Edit City, 3.Edit Vname and City");
				System.out.println("Enter the option");
				int opt =sc.nextInt();
				sc.nextLine();
				if(opt == 1) {
					System.out.println("Enter Venue Id");
					vid = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter Venue name");
					vname = sc.nextLine();
					result=controller.editVname(vid, vname);
					System.out.println((result>0)?vid+" update sucessfully ": vid+" Not Updated");
				}
				else if (opt ==2) {
					System.out.println("Enter Venue Id");
					vid = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter City");
					city = sc.nextLine();
					result=controller.editCity(vid,city);
					System.out.println((result>0)?vid+" Updated sucessfully ": vid+" Not Updated");
				}
				else if (opt == 3) {
					System.out.println("Enter Venue Id");
					vid = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter Venue name");
					vname = sc.nextLine();
					System.out.println("Enter City");
					city = sc.nextLine();
					result=controller.editVnamecity(vid, vname,city);
					System.out.println((result>0)?vid+" Updated sucessfully ": vid+" Not Updated");
				}
				
				else
					System.out.println("Invalid option");
				break;
			case 4:
				List<VenueDetails> list= controller.viewAllVenues();
				if(list.size()>0) {
					System.out.println("vid,vname,city");
				for(VenueDetails vd:list) {
					System.out.println(vd.getVid()+","+vd.getVname()+","+vd.getCity());
				}
				}else {
					System.out.println("No records found");
				}
				break;
			default:
				System.out.println("Invalid selection");
			}
		
			System.out.println("To continue press 1");
			cont=sc.nextInt();
		}while(cont==1);		
	}
	else {
			System.out.println("User id or password incorrect");
		}
	System.out.println("Done successfully!!!");
	sc.close();
	}

}
